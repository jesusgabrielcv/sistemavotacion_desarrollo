package Entidad;

import ufps.util.colecciones_seed.Pila;

public class CentroVotacion {

    private int id_centro;

    private String nombreCentro;

    private String direccion;
    
    private Pila<Mesa> mesas=new Pila();

    private int cantidadSufragantes;

    public CentroVotacion() {
    }

    public CentroVotacion(int id_centro, String nombreCentro, String direccion, int cantidad) {
        this.id_centro = id_centro;
        this.nombreCentro = nombreCentro;
        this.direccion = direccion;
        this.cantidadSufragantes=cantidad;
    }

    


    public int getId_centro() {
        return id_centro;
    }

    public void setId_centro(int id_centro) {
        this.id_centro = id_centro;
    }

    public String getNombreCentro() {
        return nombreCentro;
    }

    public void setNombreCentro(String nombreCentro) {
        this.nombreCentro = nombreCentro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Pila<Mesa> getMesas() {
        return mesas;
    }

    public void setMesas(Pila<Mesa> mesas) {
        this.mesas = mesas;
    }
    
    
    
    public void crearMesas(int cant)
    {
    //Esto por si el archivo llega a estar corrupto
    if(cant<=0)
        return;
    for(int i=1;i<=cant;i++)
        {
            this.mesas.apilar(new Mesa(i));
        }
    }

    @Override
    public String toString() {
        return "CentroVotacion{" + "id_centro=" + id_centro + ", nombreCentro=" + nombreCentro + ", direccion=" + direccion + ", cantidadSufragantes=" + cantidadSufragantes + 
                "\n Mis mesas son:"+this.getListadoMesas()+         
                '}';
    }
    
    
    private String getListadoMesas()
    {
    String msg="";
    //Este proceso es de cuidado por que se debe sacar copia de la pila
    //Esto lo deben hacer :(
    while(!this.mesas.esVacia())
        msg+=this.mesas.desapilar().toString()+"\n";
    
    return msg;
    }
    
    
    public Mesa getMesaAleatorio()
    {
    return null;
    }
    
}
